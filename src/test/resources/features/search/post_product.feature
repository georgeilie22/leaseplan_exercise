Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios


  Scenario: Verify if Coca-Cola European Partners is a cola distributors
    Given The user looks for "cola" products
    Then The results displayed contain "Coca-Cola European Partners" brand


  Scenario: Verify if mango products are not found
    Given The user looks for "mango" products
    Then The searched product is not found
