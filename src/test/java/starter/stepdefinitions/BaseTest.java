package starter.stepdefinitions;

import io.restassured.RestAssured;
import starter.utils.PropertiesUtils;

public class BaseTest {

    public void setBaseUri() {
        String env = PropertiesUtils.readProperties("src/test/resources/config.properties").getProperty("environment");

        switch (env.toLowerCase()) {
            case "test":
                RestAssured.baseURI = "https://waarkoop-server.herokuapp.com/api/";
                break;
            case "staging":
                RestAssured.baseURI = "...........";
                break;
            case "dev":
                RestAssured.baseURI = "............";
                break;
            case "prod":
                RestAssured.baseURI = ".............";
                break;
        }
    }
}
