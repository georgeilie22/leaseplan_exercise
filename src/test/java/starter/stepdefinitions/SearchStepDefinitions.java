package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import starter.requests.GetProduct;

import java.util.List;


public class SearchStepDefinitions extends BaseTest {


    @Before
    public void before(){
        setBaseUri();

    }

    @Steps
    public GetProduct productRequests;
    public Response response;

    @Given("The user looks for {string} products")
    public void getProduct(String fruit) {
         response= productRequests.getProductResponse(fruit);
    }

    @Then("The results displayed contain {string} brand")
    public void validateFruitResponse(String brand) {
        Assert.assertEquals("Request failed with status "+response.statusCode(),response.statusCode(),200);

        List<String> brands= response.body().jsonPath().getList("brand");
        Assert.assertTrue("No products found",brands.size()>0);
        Assert.assertTrue("Products list doesn't contain " + brand,brands.contains(brand));
    }

    @Then("The searched product is not found")
    public void assertFruitNotFound() {
        Assert.assertEquals("Not found error was not returned by the server",response.body().jsonPath().getString("detail.message"), "Not found");
    }
}
