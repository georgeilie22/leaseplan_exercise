package starter.requests;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class GetProduct {

    public Response getProductResponse(String fruit){
        return SerenityRest.given()
                .pathParam("product",fruit)
                .when()
                .get("v1/search/demo/{product}");
    }
}
