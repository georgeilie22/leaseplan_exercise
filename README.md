
## Project setup
To install the dependencies alone run:
```
$ mvn clean install -DskipTests
```
To run all api tests run:
```
$ mvn clean verify
```
To get the tests report follow the path:
```
target/site/serenity/index.html
```

## Adding new Tests
### Steps Definitions
Define new test steps in the `stepsdefinitions` folder located at `src/test/java/starter/stepdefinitions`.

### Feature Addition
Add new features in the `src/test/resources/features` directory.

### Environment Configuration
Switch between different environments by modifying the environment variable in the `config.properties` file located at `src/test/resources/config.properties`.

To view supported environments, refer to `BaseTest.java` in `src/test/java/starter/stepdefinitions/BaseTest.java`.

### CI/CD
The Continuous Integration/Continuous Deployment (CI/CD) configuration is specified in the `.gitlab-ci.yml` file. Tests will be automatically executed after each push on GitLab.

## Refactoring
- extracted request logic from step definition adn created a separate class for the request, so it can be reused. Find it at `src/main/java/starter/requests`
- refactored the old tests because checking it was not relevant to check if the fruit names are found in titles having in mind that some products even if they have orange aroma, they will not display that in title
- created a `BaseTest,java` that is responsible for setting the environment based on the `config.properties` file.
- assertions done are using jsonpath but for more complex data I would consider serialization and deserialization with POJOs
- setup the CI/CD to run each time new code is pushed to the repository because this way we can see fast if some old tests are broken or new bugs added. I choose to do this because api tests are fast and running them often is not a problem.
- updated `pom.xml` dependencies and added few more in order to run the rest assured tests and also because the old pom.xml configuration was not allowing me to generate reports after running the tests.
